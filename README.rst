CFLyrics
========

The **CFLyrics** library is designed to be used in UK schools to provide students with access to data that describes
the lyrics used in pop songs.

The data that **CFLyrics** accesses is provided `Lyrics Wikia <http://lyrics.wikia.com/Lyrics_Wiki>`_ and all
copyrights are acknowledged by the relevant holders. This API is designed to be used within UK schools as part of my
**CodeFurther** initiative to get kids coding in the classroom.

**CFLyrics** is under active development visit, the source code is
`here <https://bitbucket.org/dannygoodall/cflyrics`_ and is licensed under the
`Apache2 license <http://www.apache.org/licenses/LICENSE-2.0.html>`_, so feel free to
`contribute <https://bitbucket.org/dannygoodall/cflyrics/pull-requests>`_ and
`report errors and suggestions <https://bitbucket.org/dannygoodall/cflyrics/issues>`_.

.. warning::
    **CFLyrics** is not designed to be used to get around the conditions of Lyrics Wikia - which insist that your
    application should never publicly share lyrics scraped from their site, instead they stipulate that you should
    instead redirect viewers to their official site. **CFLyrics** is not designed to be used as a general tool to
    scrape lyrics, but is instead designed as teaching aid for UK schools.

Usage
-----
**CFLyrics** exposes a very simple API to developers. It is accessed by importing the :class:`~lyrics.Lyrics`
class into your module and creating an instance of this class, like so::

   from cflyrics import Lyrics
   lyrics = Lyrics()

The ``lyrics`` instance exposes a key method to the programmer:

* :py:meth:`cflyrics.getalbums <cflyrics.Lyrics.get_lyrics()>`

This accepts an artist and a title (:py:class:`str`) string, and if lyrics are found for this combination of artist and
title, it will returna  :py:class:`list` of lyrics (:py:class:`str`) to the caller.

The example code below shows how you can use one of these properties to get the lyrics for a list of the lyrics to
**Between the Wars** by **Billy Bragg**.::

    from cflyrics import Lyrics

    lyrics = Lyrics()

    song_lyrics = lyrics.get_song_lyrics("Billy Bragg", "Between the wars")

    for lyric_line in song_lyrics::
        print( lyric_line )

This short program uses the :py:meth:`~lyrics.Lyrics.get_song_lyrics` method of the :class:`~lyrics.Lyrics`
class to obtain the Python :class:`list` of strings the current Top 40 UK albums. It then loops through this list, and at each
iteration of the loop the variable `album` is set to the next album entry in the list.

A :func:`print` function then prints the lyric text :py:class:`string`, resulting in something like this:::

    I was a miner, I was a docker
    I was a railway man between the wars
    I raised a family in times of austerity
    With sweat at the foundry between the wars
    
    I paid the union and as times got harder
    I looked to the government to help the working man
    But they brought prosperity down at the armoury
    We're arming for peace me boys, between the wars
    
    I kept the faith and I kept voting
    Not for the iron fist but for the helping hand
    For theirs is a land with a wall around it
    And mine is a faith in my fellow man
    
    Theirs is a land of hope and glory
    Mine is the green field and the factory floor
    Theirs are the skies all dark with bombers
    And mine is the peace we knew between the wars
    
    Call up the craftsmen, bring me the draughtsmen
    Build me a path from cradle to grave
    And I'll give my consent to any government
    That does not deny a man a living wage
    
    Go find the young men never to fight again
    Bring up the banners from the days gone by
    Sweet moderation, heart of this nation
    Desert us not, we are between the wars


Features
========
**CFLyrics** provides:
* a list of the lyrics for a given song given an artist and song title.

Installation
============

**CFLyrics** can be found on the Python Package Index `PyPi here. <https://pypi.python.org/pypi/cflyrics>`_
It can be installed using ``pip``, like so. ::

    pip install cflyrics

Documentation
=============
The documentation for **CFLyrics** can be found on the
`ReadTheDocs site <http://cflyrics.readthedocs.org/en/latest/index.html>`_.

API - Application Programming Interface
=======================================
The full documentation of the classes and functions that make up **CFLyrics** can be found :doc:`here <lyrics>`, the
errors and exceptions can be found :doc:`here <errors>` and the utility functions are :doc:`here <utils>`.

Tests
-----
To run the **CFLyrics** test suite, you should install the test and development requirements and then run nosetests.

.. code-block:: bash

    $ pip install -r dev-requirements.txt
    $ nosetests tests

Changes
-------

See :doc:`Changes <changes>`.
