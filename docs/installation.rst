Installation
============

**CFLyrics** can be found on the Python Package Index `PyPi here. <https://pypi.python.org/pypi/cflyrics>`_
It can be installed using ``pip``, like so. ::

    pip install cflyrics