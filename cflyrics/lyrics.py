from __future__ import print_function
# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""The ``top40`` module contains the high level classes that are used to package the returned data such as
:py:class:`Entry`, :py:class:`Chart` and :py:class:`Change`.

In addition the :py:class:`Top40` class provides the main external interface into the module. Once an instance of the
:py:class:`Top40` class has been instantiated it can be used to return data from the remote API to the called program::

    from pythontop40 import Top40

    top40 = Top40()

    album_list = top40.albums
    singles_list = top40.singles

    albums_chart = top40.albums_chart
    singles_chart = top40.singles_chart

From there, the returned objects can be interrogated and interacted with::

    first_album = album_list[0]
    print( first_album.position )
    print( first_album.artist )

    print("The date of the singles chart is", singles_chart.date)
    print(The album_chart was retrieved from the server on", albums_chart.retrieved

And this, don't forget this::

    class Repo(Model):
         name = fields.String()
         owner = fields.Embedded(User)

    booby = Repo(
        name='Booby',
        owner={
            'login': 'jaimegildesagredo',
            'name': 'Jaime Gil de Sagredo'
        })

    print booby.to_json()
    '{"owner": {"login": "jaimegildesagredo", "name": "Jaime Gil de Sagredo"}, "name": "Booby"}'
"""

__author__ = 'Danny Goodall'

import requests
import requests.exceptions
from nap.url import Url
from cflyrics.errors import Top40ConnectionError, Top40Error, Top40HTTPError, Top40ReadTimeoutError


class LyricsAPI(Url):
    """Provides the physical connection to the API for the Lyrics object.

    This is the route into API and is a subclass of nap.Url. It provides the get method that carries out the request.get
    method for the chart API.

    This class overrides the :func:`ChartsAPI.get` and :func:`ChartsAPI.after_request` methods in the base class. The
    response returned from the :class:`nap.Url` class is recursively parsed to convert any embedded
    :class:`dict` objects, to :class:`~munch.Munch` types. This is to allow `dot access` to the members of the response,
    as well as using the traditional dict['key'] access.

    Attributes:
        convert (dict): contains none or more key, value pairs.
    """

    def after_request(self, response):
        if response.status_code != 200:
            response.raise_for_status()

        # Now turn the dicts in the Json response into Munch types so that
        # they can be accessed using .notation

        return response.json()


class Lyrics(object):
    """ Provides the programmer with properties that return lyrics from the Wikia site.

    The programmer creates an instance of this object, and then uses the exposed properties to access the data about
    the lyrics.

    Attributes:
        error_format (str): The format string to be used when creating error messages.
    """
    error_format = "Received an error whist reading from {}: Returned code: {}"

    def __init__(self, base_url="http://cflyricsserver.herokuapp.com/"):
        """Creates and returns the object instance.

        Args:
            base_url (str): The base url of the remote API before the specific service details are appended.
                For example, the base url might be "a.site.com/api/", and the service "/albums/", when appended to the
                base url, creates the total url required to access the album data.
        Returns:
            Top40 (Top40): The Top40 model instance.
        """
        self.api = LyricsAPI(base_url)


    def get_song_lyrics(self, artist, title):
        if artist is None or not artist or title is None or not title:
            raise ValueError("Both the artist and the title of the song you are looking for must be specified.")
        json_response = self.api.get(
            '{}/{}'.format(
                artist,
                title
            )
        )
        if "lyrics" not in json_response:
            raise ValueError("The server returned a badly assembled response.")

        # Return the :py:class:`list` of lyric strings
        return json_response['lyrics']



    def _get_data(self, url, params=None, convert=None):
        """Internal routine to retrieve data from the external service.

        The URL component that is passed is added to the base URL that was specified when the object was instantiated.
        Additional params passed will be passed to the API as key=value pairs, and the return data is parsed and
        any :func:`dict` contained within the structure is converted to a :class:`Munch` type. In addition, the convert
        :class:`dict` is used to optionally convert values returned from the API to different types.

        Args:
            url (str): The remote url to connect to.
            params (dict): Additional parameters will be passed as key=value pairs to the URL as query variables
                ?key=value.
            convert (dict): The JSON structure that is returned is parsed for instances of ``key``, and if
                found the, value of convert[key] is used to convert it. For example supplying ``{"position",int}``
                would ensure that if the key ``position`` was found in the JSON structure, it will be converted to type
                int.
        Returns:
            response (JSON): All embedded :class:`dict` will be converted to :class:`Munch`.
        """
        if not params:
            params = {}
        if not convert:
            convert = {}
        try:
            response_json = self.api.get(url, params=params, convert=convert)
        except requests.exceptions.HTTPError as e:
            message = Top40.error_format.format(
                url,
                e.response.status_code
            )
            print(message)
            raise Top40HTTPError(message, e.response.status_code)
        except (requests.exceptions.ConnectionError, requests.exceptions.SSLError, requests.exceptions.ConnectTimeout):
            raise Top40ConnectionError("Could not connect to remote server.".format(self.what_do_i_manage))
        except requests.exceptions.ReadTimeout:
            raise Top40ReadTimeoutError("The remote server took longer than expected to reply.")
        except Exception as e:
            raise
        return response_json

    def _get_albums_chart(self):
        """Internal routine to pull the albums chart information into the cache
        """
        albums = self._get_data("/albums")
        self._albums_chart = Chart(**albums)


    @property
    def albums_chart(self):
        """A ``property`` that returns the :py:class:`Chart` object for the current Top40 albums

        Returns:
            :py:class:`Chart`: The albums' chart object - an instance of the :class:`Chart` class containing the album
                information and the and the issue and retrieval dates specific to this chart.
        """
        if self._albums_chart is None:
            self._get_albums_chart()
        return self._albums_chart

    @property
    def albums(self):
        """A ``property`` that returns a :py:class:`list` of album :py:class:`Entry` types.

        Returns:
            :py:class:`list` : A :py:class:`list` of :class:`Entry` instances. Each entry describes an album in the chart.
        """
        albums_chart = self.albums_chart
        return albums_chart.entries

    def _get_singles_chart(self):
        """Internal routine to pull the singles chart information into the cache
        """
        singles = self._get_data("/singles")
        self._singles_chart = Chart(**singles)

    @property
    def singles_chart(self):
        """A ``property`` that returns the :py:class:`Chart` object for the current Top40 singles

        Returns:
            :py:class:`Chart`: The singles' chart object - an instance of the :class:`Chart` class containing the
                singles information and the issue and retrieval dates specific to this chart.
        """
        if self._singles_chart is None:
            self._get_singles_chart()
        return self._singles_chart

    @property
    def singles(self):
        """A ``property`` that returns a list of single entries.

        Returns:
            :py:class:`list`: A :py:class:`list` of :class:`Entry` instances. Each entry describes a single in the chart.
        """
        singles_chart = self.singles_chart
        return singles_chart.entries